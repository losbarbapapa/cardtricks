//
//  CardsTrickAppDelegate.h
//  CardsTrick
//
//  Created by Mirel Galan Blanco on 8/16/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "cocos2d.h"

@interface CardsTrickAppDelegate : NSObject <UIApplicationDelegate, CCDirectorDelegate, ADBannerViewDelegate>
{
	UIWindow *window_;
	UINavigationController *navController_;
    
	CCDirectorIOS	*director_;							// weak ref
    // iAds support! :D
    ADBannerView        *adview_;
}

@property (nonatomic, retain) UIWindow *window;
@property (readonly) UINavigationController *navController;
@property (readonly) CCDirectorIOS *director;
@property (nonatomic,retain) ADBannerView *adview;

@end