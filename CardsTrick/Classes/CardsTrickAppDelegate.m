//
//  MagicCardsAppDelegate.m
//  MagicCards
//
//  Created by Mirel Galan Blanco on 8/16/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

/*
#import "cocos2d.h"

#import "CardsTrickAppDelegate.h"
#import "GameConfig.h"
#import "MenuLayer.h"
#import "RootViewController.h"
#import "SettingsManager.h"
#import "SimpleAudioEngine.h"
#import "Appirater.h"

@implementation CardsTrickAppDelegate

@synthesize window;
@synthesize adview;

- (void) removeStartupFlicker
{
	//
	// THIS CODE REMOVES THE STARTUP FLICKER
	//
	// Uncomment the following code if you Application only supports landscape mode
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController

	CC_ENABLE_DEFAULT_GL_STATES();
	CCDirector *director = [CCDirector sharedDirector];
	CGSize size = [director winSize];
	CCSprite *sprite = [CCSprite spriteWithFile:@"Default.png"];
	sprite.position = ccp(size.width/2, size.height/2);
	sprite.rotation = -90;
	[sprite visit];
	[[director openGLView] swapBuffers];
	CC_ENABLE_DEFAULT_GL_STATES();
	
#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController	
}
- (void) applicationDidFinishLaunching:(UIApplication*)application
{
	// Init the window
	window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	
	// Try to use CADisplayLink director
	// if it fails (SDK < 3.1) use the default director
	if( ! [CCDirector setDirectorType:kCCDirectorTypeDisplayLink] )
		[CCDirector setDirectorType:kCCDirectorTypeDefault];
	
	
	CCDirector *director = [CCDirector sharedDirector];
	
	// Init the View Controller
	viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
	viewController.wantsFullScreenLayout = YES;
	
	//
	// Create the EAGLView manually
	//  1. Create a RGB565 format. Alternative: RGBA8
	//	2. depth format of 0 bit. Use 16 or 24 bit for 3d effects, like CCPageTurnTransition
	//
	//
	EAGLView *glView = [EAGLView viewWithFrame:[window bounds]
								   pixelFormat:kEAGLColorFormatRGBA8	// kEAGLColorFormatRGBA8
								   depthFormat:0						// GL_DEPTH_COMPONENT16_OES
						];
	
	// attach the openglView to the director
	[director setOpenGLView:glView];
	
	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	if( ! [director enableRetinaDisplay:YES] ){
		CCLOG(@"Retina Display Not supported");
    } else {
        [Globals sharedInstance].isRetina = YES;
    }
	
	//
	// VERY IMPORTANT:
	// If the rotation is going to be controlled by a UIViewController
	// then the device orientation should be "Portrait".
	//
	// IMPORTANT:
	// By default, this template only supports Landscape orientations.
	// Edit the RootViewController.m file to edit the supported orientations.
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
#else
	[director setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
#endif
	
	[director setAnimationInterval:1.0/60];
	//[director setDisplayFPS:YES];
	
	
	// make the OpenGLView a child of the view controller
	[viewController setView:glView];
	
	// make the View Controller a child of the main window
	[window addSubview: viewController.view];
	
	[window makeKeyAndVisible];
	
    adview = [[ADBannerView alloc] initWithFrame:CGRectZero];
    adview.delegate = self;
    adview.requiredContentSizeIdentifiers = [NSSet setWithObjects:ADBannerContentSizeIdentifierPortrait, ADBannerContentSizeIdentifierLandscape, nil];
    adview.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    [[[CCDirector sharedDirector] openGLView] addSubview:adview];
    // Transform iAd
    CGSize windowSize = [[CCDirector sharedDirector] winSize];
    adview.center = CGPointMake(adview.frame.size.width/2, windowSize.height/2+145);
    adview.hidden = YES;
    
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
	
	// Removes the startup flicker
	[self removeStartupFlicker];
    
    [Globals sharedInstance].language = @"en";
	[Globals sharedInstance].bActSounds = YES;
    
    // Load the saved settings(enabled sound, instructions...)
    [[SettingsManager sharedSettingsManager] load];
    if([[SettingsManager sharedSettingsManager] getInt:@"gameSounds"]==2)
        [Globals sharedInstance].bActSounds = NO;
      
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"shuffling_cards.wav"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"magic_chime.wav"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"close_button.wav"];
    
    if([[SettingsManager sharedSettingsManager] getString:@"language"]!=nil)
        [Globals sharedInstance].language = [[SettingsManager sharedSettingsManager] getString:@"language"];
    
	// Run the intro Scene
	[[CCDirector sharedDirector] runWithScene: [MenuLayer scene]];
    [Appirater appLaunched:YES];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [[SettingsManager sharedSettingsManager] save];
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[SettingsManager sharedSettingsManager] load];
	[[CCDirector sharedDirector] resume];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationDidEnterBackground:(UIApplication*)application {
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application {
	[[CCDirector sharedDirector] startAnimation];
    [Appirater appEnteredForeground:YES];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[SettingsManager sharedSettingsManager] save];
    
	CCDirector *director = [CCDirector sharedDirector];
	[[director openGLView] removeFromSuperview];
	
	[viewController release];
	[window release];
	
	[director end];	
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void)dealloc {
    adview.delegate = nil;
    [adview removeFromSuperview];
    [adview release];
    adview = nil;
    
    [[SimpleAudioEngine sharedEngine] release];
    [[SettingsManager sharedSettingsManager] release];
	[[CCDirector sharedDirector] release];
	[window release];
	[super dealloc];
}

#pragma -

-(void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    adview.hidden = NO;
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    adview.hidden = YES;
}

-(void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    [[UIApplication sharedApplication] setStatusBarOrientation:(UIInterfaceOrientation)[[CCDirector sharedDirector] deviceOrientation] ];
}

-(BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

@end
*/

#import "cocos2d.h"

#import "CardsTrickAppDelegate.h"
#import "GameConfig.h"
#import "MenuLayer.h"
#import "RootViewController.h"
#import "SettingsManager.h"
#import "SimpleAudioEngine.h"
#import "Appirater.h"

@implementation CardsTrickAppDelegate

@synthesize window=window_, navController=navController_, director=director_, adview=adview_;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	// Create the main window
	window_ = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
	// Create an CCGLView with a RGB565 color buffer, and a depth buffer of 0-bits
	CCGLView *glView = [CCGLView viewWithFrame:[window_ bounds]
								   pixelFormat:kEAGLColorFormatRGBA8
								   depthFormat:0	//GL_DEPTH_COMPONENT24_OES
							preserveBackbuffer:NO
									sharegroup:nil
								 multiSampling:NO
							   numberOfSamples:0];
    
	director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    
	director_.wantsFullScreenLayout = YES;
    
	// Display FSP and SPF
	//[director_ setDisplayStats:YES];
    
	// set FPS at 60
	[director_ setAnimationInterval:1.0/60];
    
	// attach the openglView to the director
	[director_ setView:glView];
    
	// for rotation and other messages
	[director_ setDelegate:self];
    
	// 2D projection
	[director_ setProjection:kCCDirectorProjection2D];
    //	[director setProjection:kCCDirectorProjection3D];
    
	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	if( ! [director_ enableRetinaDisplay:YES] )
		CCLOG(@"Retina Display Not supported");
    
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
    
	// If the 1st suffix is not found and if fallback is enabled then fallback suffixes are going to searched. If none is found, it will try with the name without suffix.
	// On iPad HD  : "-ipadhd", "-ipad",  "-hd"
	// On iPad     : "-ipad", "-hd"
	// On iPhone HD: "-hd"
	CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
	[sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
	[sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
	[sharedFileUtils setiPadSuffix:@"-ipad"];					// Default on iPad is "ipad"
	[sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];	// Default on iPad RetinaDisplay is "-ipadhd"
    
	// Assume that PVR images have premultiplied alpha
	[CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
    
	// Create a Navigation Controller with the Director
	navController_ = [[UINavigationController alloc] initWithRootViewController:director_];
	navController_.navigationBarHidden = YES;
	
	// set the Navigation Controller as the root view controller
	[window_ setRootViewController:navController_];
	
    // Ads
    adview_ = [[ADBannerView alloc] initWithFrame:CGRectZero];
    adview_.delegate = self;
    adview_.requiredContentSizeIdentifiers = [NSSet setWithObjects:ADBannerContentSizeIdentifierPortrait, ADBannerContentSizeIdentifierLandscape, nil];
    adview_.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    [[[CCDirector sharedDirector] view] addSubview:adview_];
    // Transform iAd
    CGSize windowSize = [[CCDirector sharedDirector] winSize];
    adview_.center = CGPointMake(adview_.frame.size.width/2, windowSize.height/2+145);
    adview_.hidden = YES;
    
    // Variables
    [Globals sharedInstance].language = @"en";
	[Globals sharedInstance].bActSounds = YES;
    
    // Load the saved settings(enabled sound, instructions...)
    [[SettingsManager sharedSettingsManager] load];
    if([[SettingsManager sharedSettingsManager] getInt:@"gameSounds"]==2)
        [Globals sharedInstance].bActSounds = NO;
    
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"shuffling_cards.wav"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"magic_chime.wav"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"close_button.wav"];
    
    if([[SettingsManager sharedSettingsManager] getString:@"language"]!=nil)
        [Globals sharedInstance].language = [[SettingsManager sharedSettingsManager] getString:@"language"];
    
	// make main window visible
	[window_ makeKeyAndVisible];
	   
	return YES;
}

// This is needed for iOS4 and iOS5 in order to ensure
// that the 1st scene has the correct dimensions
// This is not needed on iOS6 and could be added to the application:didFinish...
-(void) directorDidReshapeProjection:(CCDirector*)director
{
	if(director.runningScene == nil) {
		// Add the first scene to the stack. The director will draw it immediately into the framebuffer. (Animation is started automatically when the view is displayed.)
		// and add the scene to the stack. The director will run it when it automatically when the view is displayed.
		[director runWithScene: [MenuLayer scene]];
        [Appirater appLaunched:YES];
	}
}

// Supported orientations: Landscape. Customize it for your own needs
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


// getting a call, pause the game
-(void) applicationWillResignActive:(UIApplication *)application
{
    [[SettingsManager sharedSettingsManager] save];
	if( [navController_ visibleViewController] == director_ )
		[director_ pause];
}

// call got rejected
-(void) applicationDidBecomeActive:(UIApplication *)application
{
    [[SettingsManager sharedSettingsManager] load];
	if( [navController_ visibleViewController] == director_ )
		[director_ resume];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	if( [navController_ visibleViewController] == director_ )
		[director_ stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
	if( [navController_ visibleViewController] == director_ )
		[director_ startAnimation];
    [Appirater appEnteredForeground:YES];
}

// application will be killed
- (void)applicationWillTerminate:(UIApplication *)application
{
    [[SettingsManager sharedSettingsManager] save];
	CC_DIRECTOR_END();
}

// purge memory
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void) dealloc
{
    adview_.delegate = nil;
    [adview_ removeFromSuperview];
    [adview_ release];
    adview_ = nil;
    
    [[SimpleAudioEngine sharedEngine] release];
    [[SettingsManager sharedSettingsManager] release];
	
	[window_ release];
	[navController_ release];
    
	[super dealloc];
}

#pragma Ads

-(void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    adview_.hidden = NO;
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    adview_.hidden = YES;
}

-(void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    [[CCDirector sharedDirector] resume];
}

-(BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    [[CCDirector sharedDirector] pause];
    return YES;
}

@end