//
//  CreditsLayer.h
//  MagicCards
//
//  Created by Mirel Galan Blanco on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

// CreditsLayer Layer
@interface CreditsLayer : CCLayerColor
{
	CCSprite *gradientSprite, *creditsSprite;
    NSInvocation *callbackCLOSE;
    
	CGSize size;
	bool isPaused;
}

+(id) scene;

-(id) initWithTarget:(id)callbackObj selectorCLOSE:(SEL)selClose;
-(void) setupCredits: (ccTime) dt;
-(void) closeButtonPressed:(id) sender;

@end
