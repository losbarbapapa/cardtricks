//
//  CreditsLayer.m
//  MagicCards
//
//  Created by Mirel Galan Blanco on 08/01/2012.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CreditsLayer.h"
#import "SimpleAudioEngine.h"

// CreditsLayer implementation
@implementation CreditsLayer

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	CreditsLayer *layer = [CreditsLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) initWithTarget:(id)callbackObj selectorCLOSE:(SEL)selClose
//-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithColor:ccc4(255,255,255,255)] )) {
		self.isTouchEnabled = YES;

        NSMethodSignature *sig = [[callbackObj class] instanceMethodSignatureForSelector:selClose];
		callbackCLOSE = [NSInvocation invocationWithMethodSignature:sig];
        [callbackCLOSE setSelector:selClose];
		[callbackCLOSE setTarget:callbackObj];
		[callbackCLOSE retain];
        
		// ask director the the window size
		size = [[CCDirector sharedDirector] winSize];
        
        gradientSprite = [CCSprite spriteWithFile:@"main_bkg.png"];
		gradientSprite.position = ccp(size.width /2, size.height /2);
		[self addChild: gradientSprite];
        
		gradientSprite = [CCSprite spriteWithFile:@"credits_overlay.png"];
		gradientSprite.position = ccp(size.width /2, size.height /2);
		[self addChild: gradientSprite z:2];

        // boton cerrar
        CCMenuItemImage *closeButton = [CCMenuItemImage itemWithNormalImage:@"close.png" selectedImage:@"close.png" target:self selector:@selector(closeButtonPressed:)];
        [closeButton setPosition:ccp(0, 0)];
        
		CCMenu *menu = [CCMenu menuWithItems: closeButton, nil];
		menu.position = ccp(450,290);
		[self addChild:menu z:3];
        
        [self schedule:@selector(setupCredits:) interval:0];
	}
	return self;
}

-(void) setupCredits: (ccTime) dt {
    [self unschedule:_cmd];
    
    NSString* creditsFile = [NSString stringWithFormat:@"creditsText_%@.png",[Globals sharedInstance].language];
    creditsSprite = [CCSprite spriteWithFile:creditsFile];
    creditsSprite.position = ccp(size.width /2, size.height /2-440);
    [self addChild:creditsSprite z:1];
    
    float duration = 15.0;
    id action = [CCSequence actions:
                 [CCMoveTo actionWithDuration:duration position:ccp(size.width/2 , size.height/2+440)],
                 [CCPlace actionWithPosition: ccp(size.width/2 , size.height/2-440)],
                 nil];
    [creditsSprite runAction: [CCRepeatForever actionWithAction:action]];
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[CCDirector sharedDirector] pause];
}

-(void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[CCDirector sharedDirector] resume];
}

-(void) closeButtonPressed:(id) sender
{
    if([Globals sharedInstance].bActSounds)
        [[SimpleAudioEngine sharedEngine] playEffect:@"close_button.wav"];
    
    [callbackCLOSE invoke];
    [self removeFromParentAndCleanup:YES];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	[callbackCLOSE release];
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
