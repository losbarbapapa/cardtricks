//
//  DialogLayer.h
//
//  Created by Mirel Galan Blanco on 1/5/12.
//  Copyright 2012 GallantGames. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface DialogLayer : CCLayerColor
{
    id m_target;
    NSInvocation *callbackOK;
	NSInvocation *callbackCANCEL;
}

-(id) initWithHeader:(NSString *)header andLine1:(NSString *)line1 andLine2:(NSString *)line2 andLine3:(NSString *)line3 target:(id)callbackObj selectorOK:(SEL)selectorOK selectorCANCEL:(SEL)selectorCANCEL;
-(void) cancelButtonPressed:(id) sender;

@end
