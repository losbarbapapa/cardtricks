//
//  DialogLayer.m
//
//  Created by Mirel Galan Blanco on 1/5/12.
//  Copyright 2012 GallantGames. All rights reserved.
//

#import "DialogLayer.h"
#import "SimpleAudioEngine.h"
//#import "MainLayer.h"
//#import "SettingsManager.h"

#define HEADER_DIALOG_FONT	@"comicsans.fnt"
#define TEXT_DIALOG_FONT	@"comicsans.fnt"
#define BUTTON_DIALOG_FONT	@"comicsans.fnt"

@implementation DialogLayer

-(id) initWithHeader:(NSString *)header 
					andLine1:(NSString *)line1 
					andLine2:(NSString *)line2 
					andLine3:(NSString *)line3 
					target:(id)callbackObj 
					selectorOK:(SEL)selOK 
					selectorCANCEL:(SEL)selCANCEL
{
    if((self=[super initWithColor:ccc4(0,0,0,128)])) 
    {
		NSMethodSignature *sig = [[callbackObj class] instanceMethodSignatureForSelector:selCANCEL];
		callbackCANCEL = [NSInvocation invocationWithMethodSignature:sig];
        [callbackCANCEL setSelector:selCANCEL];
		[callbackCANCEL setTarget:callbackObj];
		[callbackCANCEL retain];
		
		CGSize screenSize = [CCDirector sharedDirector].winSize;

		CCSprite *background = [CCSprite node];
		[background initWithFile:@"dialog_bkg.png"];
		[background setPosition:ccp(screenSize.width / 2, screenSize.height / 2 + 10)];
		[self addChild:background];

        CCLabelTTF *headerShadow = [CCLabelTTF labelWithString:header fontName:RASPOUTINE_FONT fontSize:50];
		headerShadow.color = ccBLACK;
		headerShadow.opacity = 127;
		[headerShadow setPosition:ccp(243, 262)];
		[self addChild:headerShadow];

        CCLabelTTF *headerLabel = [CCLabelTTF labelWithString:header fontName:RASPOUTINE_FONT fontSize:50];
		headerLabel.color = ccWHITE;
		[headerLabel setPosition:ccp(240, 265)];
		[self addChild:headerLabel];

		//////////////////

        CCLabelTTF *line1Label = [CCLabelTTF labelWithString:line1 fontName:RASPOUTINE_FONT fontSize:35];
		//CCLabelBMFont *line1Label = [CCLabelBMFont labelWithString:line1 fntFile:TEXT_DIALOG_FONT];
		line1Label.color = ccWHITE;
		line1Label.scale = 0.84f;
		[line1Label setPosition:ccp(240, 200)];
		[self addChild:line1Label];

        CCLabelTTF *line2Label = [CCLabelTTF labelWithString:line2 fontName:RASPOUTINE_FONT fontSize:35];
		//CCLabelBMFont *line2Label = [CCLabelBMFont labelWithString:line2 fntFile:TEXT_DIALOG_FONT];
		line2Label.color = ccWHITE;
		line2Label.scale = 0.84f;
		[line2Label setPosition:ccp(240, 160)];
		[self addChild:line2Label];

        CCLabelTTF *line3Label = [CCLabelTTF labelWithString:line3 fontName:RASPOUTINE_FONT fontSize:35];
		//CCLabelBMFont *line3Label = [CCLabelBMFont labelWithString:line3 fntFile:TEXT_DIALOG_FONT];
		line3Label.color = ccWHITE;
		line3Label.scale = 0.84f;
		[line3Label setPosition:ccp(240, 120)];
		[self addChild:line3Label];

        CCMenuItemImage *closeButton = [CCMenuItemImage itemWithNormalImage:@"close.png" selectedImage:@"close.png" target:self selector:@selector(cancelButtonPressed:)];
        [closeButton setPosition:ccp(0, 0)];

		CCMenu *menu = [CCMenu menuWithItems: closeButton, nil];
		menu.position = ccp(445,280);
		[self addChild:menu];
	}
	return self;
}

-(void) cancelButtonPressed:(id) sender
{
    if([Globals sharedInstance].bActSounds)
        [[SimpleAudioEngine sharedEngine] playEffect:@"close_button.wav"];
    
    [callbackCANCEL invoke];
    [self removeFromParentAndCleanup:YES];
}
 
-(void) dealloc
 {
	[callbackCANCEL release];
	[callbackOK release];
	[super dealloc];
 }
 
@end
