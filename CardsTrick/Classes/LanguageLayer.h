//
//  LanguageLayer.h
//  CardsTrick
//
//  Created by Mirel Galan Blanco on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

// Layer inicial para elegir lenguaje
@interface LanguageLayer : CCLayer 
{
	CCMenu *menu;
}

@property (nonatomic, retain) CCMenu *menu;

+(id) scene;
-(void) menuCallbackPlay: (id) sender;

@end