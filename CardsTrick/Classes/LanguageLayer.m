//
//  LanguageLayer.m
//  CardsTrick
//
//  Created by Mirel Galan Blanco on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LanguageLayer.h"
#import "OptionsLayer.h"
#import "SimpleAudioEngine.h"
#import "SettingsManager.h"

@implementation LanguageLayer

@synthesize menu;

+(id) scene {
	
	CCScene *scene = [CCScene node];

	LanguageLayer *layer = [LanguageLayer node];
	
	[scene addChild: layer];
	
	return scene;
}

/*
 Messages:
 - cod_msg -> 0
 - cod_lang -> 1
 - msg -> 2
 */

-(id) init {
	if( (self=[super init])) {
			
		CGSize s = [[CCDirector sharedDirector] winSize];
		
		CCSprite *bkg = [CCSprite spriteWithFile:@"background.png"];
		bkg.position = ccp(s.width/2,s.height/2);
		[self addChild:bkg];
        
		self.menu = [CCMenu menuWithItems: nil];
        
        NSArray *langList = [[NSArray alloc]initWithObjects:@"English",@"Español",@"Français",nil];
        
        int fontSizeButtons = 25;
        int heightInit = 13;
		// Creando botones a partir de la base de datos...
		for(int i=0; i<[langList count]; i++) {
            CCMenuItemSprite *menuLang = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"menuButton.png"] selectedSprite:[CCSprite spriteWithFile:@"menuButton.png"] target:self selector:@selector(menuCallbackPlay:)];
            
            CCLabelTTF *labelLang = [CCLabelTTF labelWithString:[langList objectAtIndex:i] fontName:RASPOUTINE_FONT fontSize:fontSizeButtons];
            labelLang.position = ccp(s.width/2, s.height/2 + heightInit - 54*i);
            labelLang.color = ccc3(0,0,0);
            [self addChild:labelLang z:10];
            
            [self.menu addChild:menuLang];
		}
        [langList release];
		[self.menu alignItemsVerticallyWithPadding:12];
		self.menu.position = ccp(s.width/2, s.height/2 - 40);
        
		// Elastic effect
		int i=0;
		for( CCNode *child in [self.menu children] ) {
			CGPoint dstPoint = child.position;
			int offset = s.width/2 + 50;
			if( i % 2 == 0)
				offset = -offset;
			child.position = ccp( dstPoint.x + offset, dstPoint.y);
			[child runAction: 
			 [CCEaseElasticOut actionWithAction:
			[CCMoveBy actionWithDuration:2 position:ccp(dstPoint.x - offset,0)]
										period: 0.35f]
			];
			i++;
		}

		CCParticleSystemQuad *ps = [CCParticleExplosion node];
        [self addChild:ps];
        
        ps.position = ccp(s.width/2,0);
        ps.posVar = ccp(275,0);
        ps.life = 4.5;
        ps.lifeVar = 1;
        ps.gravity = ccp(-3,-10);
        ps.duration = -1;
        ps.scaleX = 1;
		ps.scaleY = 1;
		ps.rotation = 0;
		ps.emissionRate = 20;
		ps.startSize = 32;
		ps.endSize = 4;
		ps.speed = 80;
		ps.speedVar = 30;
		ps.startSpin = 0;
        ps.startSpinVar = 360;
        ps.angle = 90;
        ps.angleVar = 5;
		ps.totalParticles = 1000.0f;
		ps.autoRemoveOnFinish = YES;
                
        ps.texture = [[CCTextureCache sharedTextureCache] addImage: @"int_sign.png"];
        ps.blendAdditive = YES;

		[self addChild: self.menu];
	}

	return self;
}


-(void) menuCallbackPlay: (id) sender
{
    NSArray *langCodes = [[NSArray alloc]initWithObjects:@"en",@"es",@"fr",nil];
        
 	int ii = -1;
	for( CCNode *child in [self.menu children] ) {
        
		CCMenuItemLabel *menuPlay = (CCMenuItemLabel*)child; 
		CCMenuItemLabel *menuPlay2 = (CCMenuItemLabel*)sender; 
		ii++;
		if (menuPlay == menuPlay2)
		{
            [Globals sharedInstance].language = [langCodes objectAtIndex:ii];
			break;
		}
	}
    [langCodes release];
    [[SettingsManager sharedSettingsManager] setValue:@"language" newString:[Globals sharedInstance].language];
    [[SettingsManager sharedSettingsManager] save];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1 scene:[OptionsLayer node]]];
}

- (void) dealloc
{
	[menu release];
	[super dealloc];
}

@end
