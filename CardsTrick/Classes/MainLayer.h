//
//  MainLayer.h
//  MagicCards
//
//  Created by Mirel Galan Blanco on 8/16/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface MainLayer : CCLayer
{
	NSMutableArray *completeList;
	NSMutableArray *cols;
	NSMutableArray *controlArray;

	int cantRepartidas;
	CGSize size;
	
    CCSprite* backButton;
	CCSprite* restartButton;
    CCSprite* helpButton;
    
    bool bEstJugable;
}

// returns a CCScene that contains the MainLayer as the only child
+(CCScene *) scene;

-(void) repartirCartas;
-(void) insertarCartas;
-(void) columnaSeleccionada:(int) col1 columna2:(int) col2 columna3:(int) col3;
-(void) tapDown:(CGPoint)location;
-(void) onClickCol1:(id) sender;
-(void) onClickCol2:(id) sender;
-(void) onClickCol3:(id) sender;
-(void) onClickCloseTut:(id) sender;
-(void) onClickCloseCredits:(id) sender;
-(void) addUtilsButtons;
-(void) showHelp:(int) step;

@end
