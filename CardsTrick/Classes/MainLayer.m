//
//  MainLayer.m
//  MagicCards
//
//  Created by Mirel Galan Blanco on 8/16/11.
//  Copyright GallantGames 2011. All rights reserved.
//


// Import the interfaces
#import "MainLayer.h"
#import "MenuLayer.h"
#import "DialogLayer.h"
#import "CreditsLayer.h"

#import "SettingsManager.h"
#import "SimpleAudioEngine.h"

enum zOrderElems{
    kTagWinnerLab = 1,
    kTagWinnerCard,
    kTagWinnerPar,
    kTagButtons,
	kTagCartas
};

#define SIZE_UTILS_BUTTONS 32
#define ESCALA_CARTAS 0.4f
#define ESCALA_CARTAS_RETINA 0.8f

// MainLayer implementation
@implementation MainLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainLayer *layer = [MainLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	// Llamar siempre "super".init
	// Apple recomienda reasignar el "self"
	if( (self=[super init/*WithColor:ccc4(39,167,213,255)*/])) { // le pongo color de fondo
		self.isTouchEnabled = YES;
		bEstJugable = YES;
        
        CGSize s = [[CCDirector sharedDirector] winSize];
		
		CCSprite *bkg = [CCSprite spriteWithFile:@"main_bkg.png"];
		bkg.position = ccp(s.width/2,s.height/2);
		[self addChild:bkg];
        
        // inicializo arreglos
		completeList = [[NSMutableArray alloc] initWithCapacity:21];
		cols = [[NSMutableArray alloc] initWithCapacity:3];
		[cols addObject:[[NSMutableArray alloc] init]];
		[cols addObject:[[NSMutableArray alloc] init]];
		[cols addObject:[[NSMutableArray alloc] init]];
		controlArray = [[NSMutableArray alloc] initWithCapacity:21];
		
        // genero cartas aleatoriamente
		int i=0;
		for (; i < 21; i++)
		{
			int carta = (arc4random() % 54) + 1;
			NSString *sCarta = [NSString stringWithFormat:@"%d",carta];
			while ([controlArray containsObject:sCarta]){
				carta = (arc4random() % 54) + 1;
				sCarta = [NSString stringWithFormat:@"%d",carta];
			}
            NSLog(@"%@",sCarta);
            
			//[self insertarCarta:carta];
			[completeList addObject:sCarta];
			[controlArray addObject:sCarta];
		}
        
        restartButton = nil;
        backButton = nil;
        helpButton = nil;
        
        // pongo las cartas
        [self repartirCartas];
               
        [self addUtilsButtons];
        
        // Muestro primer dialogo de ayuda
        int showedFirstTut = [[SettingsManager sharedSettingsManager] getInt:@"first_tut"];
        
        if(showedFirstTut != 1){
            [self showHelp:1];
            
            self.isTouchEnabled = NO;
            bEstJugable = NO;
        }
	}
	return self;
}

// pongo botones: back, restart, help
-(void) addUtilsButtons {
    
    if(restartButton==nil){
        restartButton = [[CCSprite spriteWithFile:@"reset.png"] retain];
        restartButton.position = ccp(size.width-60, size.height-24);
    }
    [self addChild:restartButton z:kTagButtons tag:kTagButtons];
    
    if(helpButton==nil){
        helpButton = [[CCSprite spriteWithFile:@"help.png"] retain];
        helpButton.position = ccp(size.width-24, size.height-24);
    }
    [self addChild:helpButton z:kTagButtons tag:kTagButtons];
    
    if(backButton==nil){
        backButton = [[CCSprite spriteWithFile:@"back.png"] retain];
        backButton.position = ccp(24, size.height-24);
    }
    [self addChild:backButton z:kTagButtons tag:kTagButtons];
}

// Reparto cartas en los arreglos y las muestro...
-(void) repartirCartas {
	
	int i = 0;
	for ( ; i < 3; i++) [[cols objectAtIndex:i] removeAllObjects];
	
	int colActual = 0;
	for (i = 0; i < 21; i++)
	{
		if (colActual == 3) colActual = 0;
		NSString *sCarta = [completeList objectAtIndex:i];
		[[cols objectAtIndex:colActual] addObject:sCarta];
		colActual++;
	}

	[self insertarCartas];
    //[self addUtilsButtons];
}

-(void) insertarCartas {
	
	size = [[CCDirector sharedDirector] winSize];
	int h, w;

    //[self removeChildByTag:kTagCartas cleanup:YES];
    for (int i = kTagCartas; i <= kTagCartas + 21; i++)
        [self removeChildByTag:i cleanup:YES];
    int tagsCartas = kTagCartas;
    
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 7; j++) {
			
			NSString *carta = [NSString stringWithFormat:@"%@.png", [[cols objectAtIndex:i] objectAtIndex:j]];
			CCSprite *sprite = [CCSprite spriteWithFile:carta];
			
			//CCLabelTTF *label = [CCLabelTTF labelWithString:[[cols objectAtIndex:i] objectAtIndex:j] fontName:@"Marker Felt" fontSize:14];
			
			h = size.height/1.3 - j*25 - 10;
			w = (size.width - (size.width/4)*(i+1));
	
			switch (i) {
				case 0:
					w = (size.width - (size.width/4)*3);
					break;
				case 1:
					w = (size.width - (size.width/4)*2);
					break;
				case 2:
					w = (size.width - (size.width/4));
					break;
				default:
					break;
			}

			sprite.position = ccp( w , h );
			[self addChild:sprite z:kTagCartas tag:tagsCartas++];
            [sprite runAction: [CCScaleTo actionWithDuration:1 scale:((![Globals sharedInstance].isRetina) ? ESCALA_CARTAS : ESCALA_CARTAS_RETINA)]];
		}
	}
}

// Acciones cuando se selecciona una columna...
-(void) columnaSeleccionada:(int) col1 columna2:(int) col2 columna3:(int) col3 {
	cantRepartidas++;
	
    [completeList removeAllObjects];
    
    for (int i = 0; i < 7; i++)
        [completeList addObject:[[cols objectAtIndex:col2] objectAtIndex:i]];
    for (int i = 0; i < 7; i++)
        [completeList addObject:[[cols objectAtIndex:col1] objectAtIndex:i]];
    for (int i = 0; i < 7; i++)
        [completeList addObject:[[cols objectAtIndex:col3] objectAtIndex:i]];
    
	if (cantRepartidas == 3) // SE ACABO EL JUEGO!!!
	{
        bEstJugable = NO;
		cantRepartidas = 0;
        
        for (int i = kTagCartas; i <= kTagCartas + 21; i++)
            [self removeChildByTag:i cleanup:YES];

		CCLabelTTF *label = [CCLabelTTF labelWithString:[[Globals sharedInstance] languageSelectedStringForKey:@"finalCard"] fontName:RASPOUTINE_FONT fontSize:30];
		label.position =  ccp( size.width/2 , size.height/2 + 80);
		// add the label as a child to this Layer
		[self addChild:label z:kTagWinnerLab tag:kTagWinnerLab];
		
		NSString *carta = [NSString stringWithFormat:@"%@.png", [completeList objectAtIndex:10]];
		CCSprite *sprite = [CCSprite spriteWithFile:carta];
	
        sprite.position = ccp(size.width/2, size.height/2);
   		[self addChild:sprite z:kTagWinnerLab tag:kTagWinnerCard];
        [sprite runAction: [CCScaleTo actionWithDuration:0.5 scale:((![Globals sharedInstance].isRetina) ? ESCALA_CARTAS : ESCALA_CARTAS_RETINA)]];
              
        if([Globals sharedInstance].bActSounds)
            [[SimpleAudioEngine sharedEngine] playEffect:@"magic_chime.wav"];
        
        CCParticleSystem *ps = [CCParticleExplosion node]; 
        [self addChild:ps z:kTagWinnerLab tag:kTagWinnerPar];
        ps.texture = [[CCTextureCache sharedTextureCache] addImage:carta];
        ps.blendAdditive = YES;
        ps.scaleX = 3;
        ps.scaleY = 4;
        ps.speed = 10;
        ps.totalParticles = 10.0f;
        ps.autoRemoveOnFinish = YES; 
	} else {
        [self repartirCartas];
        // Muestro primer dialogo de ayuda
        int showedTut = [[SettingsManager sharedSettingsManager] getInt:((cantRepartidas == 1) ? @"second_tut" : @"third_tut")];
             
        if(showedTut != 1){
            [self showHelp:cantRepartidas+1];
            
            self.isTouchEnabled = NO;
            bEstJugable = NO;
        } else {
            if([Globals sharedInstance].bActSounds && (cantRepartidas==1 || cantRepartidas == 2))
                [[SimpleAudioEngine sharedEngine] playEffect:@"shuffling_cards.wav"];
        }
    }
}

-(void) showHelp:(int) step
{
    NSString * title = [[Globals sharedInstance] languageSelectedStringForKey:@"instructions"];
    NSString* lines = [[Globals sharedInstance] languageSelectedStringForKey:[NSString stringWithFormat:@"inst%i", step]];
    
    NSArray *linesArray = [lines componentsSeparatedByString:@"***"];
    NSString* line1 = [linesArray objectAtIndex:0];
    NSString* line2 = [linesArray objectAtIndex:1];
    NSString* line3 = [linesArray objectAtIndex:2];
    
    CCLayer *dialogLayer = [[[DialogLayer alloc]
                             initWithHeader:title
                             andLine1:line1
                             andLine2:line2
                             andLine3:line3
                             target:self
                             selectorOK:@selector(onClickCloseTut:)
                             selectorCANCEL:@selector(onClickCloseTut:)] autorelease];
    [self addChild:dialogLayer z:200];
}

- (void)tapDown:(CGPoint)location {
	   
	// reset button
	CGRect rect = CGRectMake(restartButton.position.x-SIZE_UTILS_BUTTONS/2, restartButton.position.y-SIZE_UTILS_BUTTONS/2+self.position.y, SIZE_UTILS_BUTTONS, SIZE_UTILS_BUTTONS);
	NSLog(@"%f - %f", location.x, location.y);
    
	if(CGRectContainsPoint(rect, location)) {
		cantRepartidas = 0;
		//[self removeChildByTag:kTagCartas cleanup:YES];
		for (int i = kTagCartas; i <= kTagCartas + 21; i++)
            [self removeChildByTag:i cleanup:YES];
        [self removeChildByTag:kTagWinnerLab cleanup:YES];
        [self removeChildByTag:kTagWinnerCard cleanup:YES];
        [self removeChildByTag:kTagWinnerPar cleanup:YES];
        
		[controlArray removeAllObjects];
		[completeList removeAllObjects];
		int i=0;
		for (; i < 21; i++)
		{
			int carta = (arc4random() % 52) + 1;
			NSString *sCarta = [NSString stringWithFormat:@"%d",carta];
			while ([controlArray containsObject:sCarta]){
				carta = (arc4random() % 52) + 1;
				sCarta = [NSString stringWithFormat:@"%d",carta];
			}

			[completeList addObject:sCarta];
			[controlArray addObject:sCarta];
		}
		[self repartirCartas];
        bEstJugable = YES;
		return;
	}
    
    // help button
	rect = CGRectMake(helpButton.position.x-SIZE_UTILS_BUTTONS/2, backButton.position.y-SIZE_UTILS_BUTTONS/2+self.position.y, SIZE_UTILS_BUTTONS, SIZE_UTILS_BUTTONS);
	
	if(CGRectContainsPoint(rect, location)) {
        // Muestro primer dialogo de ayuda
        if(bEstJugable){
            [self showHelp:cantRepartidas+1];
        } else {
            CCLayer *creditsLayer = [[[CreditsLayer alloc] initWithTarget:self selectorCLOSE:@selector(onClickCloseCredits:)] autorelease];
            [self addChild:creditsLayer z:200];
        }
        
        self.isTouchEnabled = NO;
        bEstJugable = NO;
    }
    
    // back button
	rect = CGRectMake(backButton.position.x-SIZE_UTILS_BUTTONS/2, backButton.position.y-SIZE_UTILS_BUTTONS/2+self.position.y, SIZE_UTILS_BUTTONS, SIZE_UTILS_BUTTONS);
	
	if(CGRectContainsPoint(rect, location)) 
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1 scene:[MenuLayer node]]];
    
    if(bEstJugable){
        // col 1
        int i=2,j=6;
        float y = size.height/1.3 - j*25 - 10;
        float x = (size.width - (size.width/4)*(i+1));
        //rect = CGRectMake(86, 45, 70, 250);
        rect = CGRectMake(x - (70/2), 45, 70, 250);
        NSLog(@"1) %f - %f", x, y);
        
        if(CGRectContainsPoint(rect, location)) 
            [self onClickCol1:nil];
    
        // col 2
        i=1;
        y = size.height/1.3 - j*25 - 10;
        x = (size.width - (size.width/4)*(i+1));
        //rect = CGRectMake(207, 45, 70, 250);
        rect = CGRectMake(x - (70/2), 45, 70, 250);
        NSLog(@"2) %f - %f", x, y);
        
        if(CGRectContainsPoint(rect, location)) 
            [self onClickCol2:nil];
    
        // col 3
        i=0;
        y = size.height/1.3 - j*25 - 10;
        x = (size.width - (size.width/4)*(i+1));
        //rect = CGRectMake(327, 45, 70, 250);
        rect = CGRectMake(x - (70/2), 45, 70, 250);
        NSLog(@"3) %f - %f", x, y);
        
        if(CGRectContainsPoint(rect, location)) 
            [self onClickCol3:nil];
    }
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch* touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	location = [self convertToNodeSpace:location];
	NSLog(@"ENTRA AL TOUCH!!!");
	[self tapDown:location];
	//return true;
}

-(void) onClickCol1:(id) sender{
	[self columnaSeleccionada:0 columna2:1 columna3:2];
}

-(void) onClickCol2:(id) sender{
	[self columnaSeleccionada:1 columna2:0 columna3:2];
}

-(void) onClickCol3:(id) sender{
	[self columnaSeleccionada:2 columna2:0 columna3:1];
}

-(void) onClickCloseTut:(id) sender{
    NSString *tut = (cantRepartidas == 0) ? @"first_tut" : ((cantRepartidas == 1) ? @"second_tut" : @"third_tut");
    [[SettingsManager sharedSettingsManager] setValue:tut newInt:1];
    [[SettingsManager sharedSettingsManager] save];
    
    self.isTouchEnabled = YES;
    bEstJugable = YES;
}

-(void) onClickCloseCredits:(id) sender{
    self.isTouchEnabled = YES;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [restartButton release];
    [helpButton release];
    [backButton release];
    
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
    [completeList release];
    for(int i = 0; i < 3; i++)
        [[cols objectAtIndex:i] release];
    [cols release];
    //[cols dealloc];
    [controlArray release];
    	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
