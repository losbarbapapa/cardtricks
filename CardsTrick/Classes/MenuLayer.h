//
//  MenuLayer.h
//  Tresillo
//
//  Created by Mirel Galan Blanco on 6/22/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import "cocos2d.h"

/** A sample application-specific CC3Layer subclass. */
@interface MenuLayer : CCLayer 
{
}

+(id) scene;

-(void) menuCallbackPlay:(id) sender;
-(void) menuCallbackOptions:(id) sender;
-(void) menuCallbackRate:(id) sender;

@end
