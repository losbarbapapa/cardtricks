//
//  MenuLayer.m
//  CardsTrick
//
//  Created by Mirel Galan Blanco on 6/22/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import "MenuLayer.h"
#import "MainLayer.h"
#import "OptionsLayer.h"

@implementation MenuLayer

+(id) scene {
	
	CCScene *scene = [CCScene node];
	MenuLayer *layer = [MenuLayer node];
	
	[scene addChild: layer];
	
	return scene;
}

-(id) init {
	if( (self=[super init])) {
		CGSize s = [[CCDirector sharedDirector] winSize];
		
		CCSprite *bkg = [CCSprite spriteWithFile:@"background.png"];
		bkg.position = ccp(s.width/2,s.height/2);
		[self addChild:bkg];
		
        int fontSizeButtons = 25;
        int heightInit = 13;
        
        // Play button
        NSString *playText = [[Globals sharedInstance] languageSelectedStringForKey:@"play"];
        CCMenuItemSprite *menuPlay = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"menuButton.png"] selectedSprite:[CCSprite spriteWithFile:@"menuButton.png"] target:self selector:@selector(menuCallbackPlay:)];
        CCLabelTTF *labelPlay = [CCLabelTTF labelWithString:playText fontName:RASPOUTINE_FONT fontSize:fontSizeButtons];
        labelPlay.position = ccp(s.width/2, s.height/2 + heightInit);
        labelPlay.color = ccc3(0, 0, 0);
        [self addChild:labelPlay z:10];
        
        // Options button
        NSString *optionsText = [[Globals sharedInstance] languageSelectedStringForKey:@"options"];
        CCMenuItemSprite *menuOptions = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"menuButton.png"] selectedSprite:[CCSprite spriteWithFile:@"menuButton.png"] target:self selector:@selector(menuCallbackOptions:)];
        CCLabelTTF *labelOptions = [CCLabelTTF labelWithString:optionsText fontName:RASPOUTINE_FONT fontSize:fontSizeButtons];
        labelOptions.position = ccp(s.width/2, s.height/2 + heightInit-54);
        labelOptions.color = ccc3(0, 0, 0);
        [self addChild:labelOptions z:10];

        // Rate button
        NSString *rateText = [[Globals sharedInstance] languageSelectedStringForKey:@"rateApp"];
        CCMenuItemSprite *menuRate = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"menuButton.png"] selectedSprite:[CCSprite spriteWithFile:@"menuButton.png"] target:self selector:@selector(menuCallbackRate:)];
        CCLabelTTF *labelRate = [CCLabelTTF labelWithString:rateText fontName:RASPOUTINE_FONT fontSize:fontSizeButtons];
        labelRate.position = ccp(s.width/2, s.height/2 + heightInit-108);
        labelRate.color = ccc3(0, 0, 0);
        [self addChild:labelRate z:10];

		
		CCMenu *menu = [CCMenu menuWithItems: menuPlay, menuOptions, menuRate, nil];
		[menu alignItemsVerticallyWithPadding:12];
		menu.position = ccp(s.width/2, s.height/2 - 40);
		
		// elastic effect
		int i=0;
		for( CCNode *child in [menu children] ) {
			CGPoint dstPoint = child.position;
			int offset = s.width/2 + 50;
			if( i % 2 == 0)
				offset = -offset;
			child.position = ccp( dstPoint.x + offset, dstPoint.y);
			[child runAction: 
			 [CCEaseElasticOut actionWithAction:
			  [CCMoveBy actionWithDuration:2 position:ccp(dstPoint.x - offset,0)]
										 period: 0.35f]
			 ];
			i++;
		}
		
		CCParticleSystemQuad *ps = [CCParticleExplosion node];
        [self addChild:ps];
        
        ps.position = ccp(s.width/2,0);
        ps.posVar = ccp(275,0);
        ps.life = 4.5;
        ps.lifeVar = 1;
        ps.gravity = ccp(-3,-10);
        ps.duration = -1;
        ps.scaleX = 1;
		ps.scaleY = 1;
		ps.rotation = 0;
		ps.emissionRate = 20;
		ps.startSize = 32;
		ps.endSize = 4;
		ps.speed = 80;
		ps.speedVar = 30;
		ps.startSpin = 0;
        ps.startSpinVar = 360;
        ps.angle = 90;
        ps.angleVar = 5;
		ps.totalParticles = 1000.0f;
		ps.autoRemoveOnFinish = YES;
        
                
        ps.texture = [[CCTextureCache sharedTextureCache] addImage: @"int_sign.png"];
        ps.blendAdditive = YES;
        
		[self addChild: menu];
	}
	
	return self;
}


-(void) menuCallbackPlay: (id) sender
{
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1 scene:[MainLayer node]]];
}

-(void) menuCallbackOptions:(id) sender
{
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1 scene:[OptionsLayer node]]];
}

-(void) menuCallbackRate: (id) sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=494773095"]];
}

- (void)dealloc {
    [super dealloc];
}

@end
