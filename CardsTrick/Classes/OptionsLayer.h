//
//  OptionsLayer.h
//  Tresillo
//
//  Created by Mirel Galan Blanco on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

/** A sample application-specific CC3Layer subclass. */
@interface OptionsLayer : CCLayer 
{
	CCSprite *soundIcon;
}

-(void) menuCallbackSound:  (id) sender;
-(void) menuCallbackLanguage: (id) sender;
-(void) menuCallbackMusic:  (id) sender;
-(void) backCallback:       (id) sender;
-(void) menuCallbackDisabled:(id) sender;

@end