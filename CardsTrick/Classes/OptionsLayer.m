//
//  OptionsLayer.m
//  CardsTrick
//
//  Created by Mirel Galan Blanco on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OptionsLayer.h"
#import "MenuLayer.h"
#import "SettingsManager.h"
#import "LanguageLayer.h"

#import "SimpleAudioEngine.h"

@implementation OptionsLayer

-(id) init {
	if( (self = [super init] ) ) {
		CGSize s = [[CCDirector sharedDirector] winSize];
        
		CCSprite *bkg = [CCSprite spriteWithFile:@"background.png"];
		bkg.position = ccp(s.width/2,s.height/2);
		[self addChild:bkg];
				
        int fontSizeButtons = 25;
        int heightInit = 13;
        
        // Sound button
        NSString *soundText = [[Globals sharedInstance] languageSelectedStringForKey:@"sound"];
        CCMenuItemSprite *menuSound = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"menuButton.png"] selectedSprite:[CCSprite spriteWithFile:@"menuButton.png"] target:self selector:@selector(menuCallbackSound:)];
        
        CCLabelTTF *labelSound = [CCLabelTTF labelWithString:soundText fontName:RASPOUTINE_FONT fontSize:fontSizeButtons];
        labelSound.position = ccp(s.width/2, s.height/2 + heightInit);
        labelSound.color = ccc3(0,0,0);
        [self addChild:labelSound z:10];
        
        if([Globals sharedInstance].bActSounds)
            soundIcon = [CCSprite spriteWithFile:@"soundOn.png"];
        else
            soundIcon = [CCSprite spriteWithFile:@"soundOff.png"];
        soundIcon.position =  ccp(s.width/2 + 55, s.height/2 + 13);
        soundIcon.rotation = -10;
        [self addChild:soundIcon z:30];
        
		
        // Language button
        NSString *langText = [[Globals sharedInstance] languageSelectedStringForKey:@"lang"];
        CCMenuItemSprite *menuLang = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"menuButton.png"] selectedSprite:[CCSprite spriteWithFile:@"menuButton.png"] target:self selector:@selector(menuCallbackLanguage:)];
        
        CCLabelTTF *labelLang = [CCLabelTTF labelWithString:langText fontName:RASPOUTINE_FONT fontSize:fontSizeButtons];
        labelLang.position = ccp(s.width/2, s.height/2 + heightInit - 54);
        labelLang.color = ccc3(0,0,0);
        [self addChild:labelLang z:10];
 
		// Back button
        NSString *backText = [[Globals sharedInstance] languageSelectedStringForKey:@"back"];
        CCMenuItemSprite *menuBack = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"menuButton.png"] selectedSprite:[CCSprite spriteWithFile:@"menuButton.png"] target:self selector:@selector(backCallback:)];
        
        CCLabelTTF *labelBack = [CCLabelTTF labelWithString:backText fontName:RASPOUTINE_FONT fontSize:fontSizeButtons];
        labelBack.position = ccp(s.width/2, s.height/2 + heightInit - 108);
        labelBack.color = ccc3(0,0,0);
        [self addChild:labelBack z:10];
 
		CCMenu *menu = [CCMenu menuWithItems:menuSound, menuLang, menuBack, nil];
        
        [menu alignItemsVerticallyWithPadding:12];
		menu.position = ccp(s.width/2, s.height/2 - 40);
		[self addChild: menu];
        
        // elastic effect
		int i=0;
		for( CCNode *child in [menu children] ) {
			CGPoint dstPoint = child.position;
			int offset = s.width/2 + 50;
			if( i % 2 == 0)
				offset = -offset;
			child.position = ccp( dstPoint.x + offset, dstPoint.y);
			[child runAction: 
			 [CCEaseElasticOut actionWithAction:
			  [CCMoveBy actionWithDuration:2 position:ccp(dstPoint.x - offset,0)]
										 period: 0.35f]
			 ];
			i++;
		}
        
        CCParticleSystemQuad *ps = [CCParticleExplosion node];
        [self addChild:ps];
        
        ps.position = ccp(s.width/2,0);
        ps.posVar = ccp(275,0);
        ps.life = 4.5;
        ps.lifeVar = 1;
        ps.gravity = ccp(-3,-10);
        ps.duration = -1;
        ps.scaleX = 1;
		ps.scaleY = 1;
		ps.rotation = 0;
		ps.emissionRate = 20;
		ps.startSize = 32;
		ps.endSize = 4;
		ps.speed = 80;
		ps.speedVar = 30;
		ps.startSpin = 0;
        ps.startSpinVar = 360;
        ps.angle = 90;
        ps.angleVar = 5;
		ps.totalParticles = 1000.0f;
		ps.autoRemoveOnFinish = YES;
                
        ps.texture = [[CCTextureCache sharedTextureCache] addImage: @"int_sign.png"];
        ps.blendAdditive = YES;
	}
	
	return self;
}

- (void) dealloc
{
	[super dealloc];
}

-(void) menuCallbackSound: (id) sender
{
	CCTexture2D* tex = [[CCTextureCache sharedTextureCache] addImage:[Globals sharedInstance].bActSounds ? @"soundOff.png" : @"soundOn.png"];
	[soundIcon setTexture: tex];
		
    if([Globals sharedInstance].bActSounds) {
        //[[SimpleAudioEngine sharedEngine] playEffect:@"button.wav"];
        [[SettingsManager sharedSettingsManager] setValue:@"gameSounds" newInt:2];
        [Globals sharedInstance].bActSounds = NO;
        
    } else {
        [[SimpleAudioEngine sharedEngine] playEffect:@"magic_chime.wav"];
        [[SettingsManager sharedSettingsManager] setValue:@"gameSounds" newInt:1];
        [Globals sharedInstance].bActSounds = YES;
    }
    
    [[SettingsManager sharedSettingsManager] save];
    //[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0 scene:[OptionsLayer node]]];
}

-(void) menuCallbackLanguage: (id) sender
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1 scene:[LanguageLayer node]]];
}

// No tiene background music aun TODO: agregarlo!!!
-(void) menuCallbackMusic: (id) sender
{
    int value = (unsigned int) [sender selectedIndex];
	if (value == 1)
		[[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
	else 
		[[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
    
    [[SettingsManager sharedSettingsManager] setValue:@"backMusic" newInt:(value==1 ? 2 : 1)];
	[[SettingsManager sharedSettingsManager] save];
}

-(void) backCallback: (id) sender
{
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1 scene:[MenuLayer node]]];
}

-(void) menuCallbackDisabled:(id) sender{}

@end

