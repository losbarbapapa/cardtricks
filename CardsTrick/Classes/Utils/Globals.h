//
//  Globals.h
//  iHanoi
//
//  Created by Mirel Galan Blanco on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Globals : NSObject {
	NSString* language;
    bool isRetina;
    bool bActSounds;
    bool bActMusic;
    int currentLevel;
}

@property (nonatomic,retain) NSString *language;
@property (nonatomic) bool isRetina;
@property (nonatomic) bool bActSounds;
@property (nonatomic) bool bActMusic;
@property (nonatomic) int currentLevel;

// Singleton
+(Globals *)sharedInstance;
-(NSString*) languageSelectedStringForKey:(NSString*) key;

@end
