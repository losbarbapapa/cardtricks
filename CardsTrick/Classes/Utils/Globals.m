//
//  Globals.m
//  iHanoi
//
//  Created by Mirel Galan Blanco on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Globals.h"

@implementation Globals

@synthesize language;
@synthesize isRetina;
@synthesize bActSounds;
@synthesize bActMusic;
@synthesize currentLevel;

+ (Globals *)sharedInstance
{
    // Instancia de la clase
    static Globals *myInstance = nil;
	
    // Si no existe la instancia la creo
    if (nil == myInstance) {
        myInstance  = [[[self class] alloc] init];
    }
    
    // Retorno mi instancia
    return myInstance;
}

-(NSString*) languageSelectedStringForKey:(NSString*) key
{    
	NSString *path;
	if([language isEqualToString:@"en"])
		path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
	else if([language isEqualToString:@"es"])
		path = [[NSBundle mainBundle] pathForResource:@"es" ofType:@"lproj"];
	else if([language isEqualToString:@"fr"])
		path = [[NSBundle mainBundle] pathForResource:@"fr" ofType:@"lproj"];
    
	NSBundle* languageBundle = [NSBundle bundleWithPath:path];
	NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
	return str;
}

@end
