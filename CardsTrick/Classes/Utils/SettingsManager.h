//
//  SettingsManager.h
//  iHanoi
//
//  Created by Mirel Galan Blanco on 1/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingsManager : NSObject {
	NSMutableDictionary* settings;
}

-(NSString *)getString:(NSString*)value;
-(int)getInt:(NSString*)value;
-(NSMutableDictionary*)getDic:(NSString*)value;

-(void)setValue:(NSString*)value newString:(NSString *)aValue;
-(void)setValue:(NSString*)value newInt:(int)aValue;
-(void)setValue:(NSString*)value newDic:(NSMutableDictionary*)aValue;

-(void)save;
-(void)load;
-(void)logSettings;

+(SettingsManager*)sharedSettingsManager;

@end
