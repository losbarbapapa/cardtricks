//
//  SettingsManager.m
//  iHanoi
//
//  Created by Mirel Galan Blanco on 1/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingsManager.h"

@implementation SettingsManager

-(NSString *)getString:(NSString*)value
{	
	return [settings objectForKey:value];
}

-(int)getInt:(NSString*)value {
	return [[settings objectForKey:value] intValue];
}

-(NSMutableDictionary*)getDic:(NSString*)value {
    return (NSMutableDictionary*)[settings objectForKey:value];
}

-(void)setValue:(NSString*)value newString:(NSString *)aValue {	
	[settings setObject:aValue forKey:value];
}

-(void)setValue:(NSString*)value newInt:(int)aValue {
	[settings setObject:[NSString stringWithFormat:@"%i",aValue] forKey:value];
}

-(void)setValue:(NSString*)value newDic:(NSMutableDictionary*)aValue {
	[settings setObject:(id)aValue forKey:value];
}

-(void)save
{
	[[NSUserDefaults standardUserDefaults] setObject:settings forKey:@"iHanoi"];
	[[NSUserDefaults standardUserDefaults] synchronize];	
}

-(void)load
{
	[settings addEntriesFromDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"iHanoi"]];
}

// Logging function great for checkin out what keys/values you have at any given time
//
-(void)logSettings
{
	for(NSString* item in [settings allKeys])
	{
		NSLog(@"[SettingsManager KEY:%@ - VALUE:%@]", item, [settings valueForKey:item]);
	}
}

+(SettingsManager*)sharedSettingsManager
{
    // Instancia de la clase
    static SettingsManager *myInstance = nil;
	
    // Si no existe la instancia la creo
    if (nil == myInstance) {
        myInstance  = [[[self class] alloc] init];
    }
    
    // Retorno mi instancia
    return myInstance;
}

-(id)init {	
	settings = [[NSMutableDictionary alloc] initWithCapacity:5];	
	return [super init];
}

@end