//
//  main.m
//  CardsTrick
//
//  Created by Mirel Galan Blanco on 1/11/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"CardsTrickAppDelegate");
    [pool release];
    return retVal;
}
